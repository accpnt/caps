extern void lowerString(char* filename);
extern void upperString(char* filename);
extern void upper1stWord(char* filename);
extern void lower1stLetters(char* filename);
extern void addUnderscores(char* filename);
extern void removeUnderscores(char* filename);
