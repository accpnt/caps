#include <windows.h>

#include "resource.h"
#include "shlobj.h"

HBITMAP g_hbmBall = NULL;

BOOL lower        = TRUE;
BOOL upper        = FALSE;
BOOL capitalize   = FALSE;
BOOL title        = FALSE;

BOOL underscores  = FALSE;
BOOL spaces       = FALSE;
BOOL merge_spaces = FALSE;
BOOL recurse      = FALSE;

char status[6] = {'L',' ',' ',' ',' ',' '};



void DoFileOpen(HWND hwnd)
{
	OPENFILENAME ofn;
	char szFileName[MAX_PATH] = "";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = "All Files (*.*)\0*.*\0";
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = "mp3";

	if(GetOpenFileName(&ofn))
	{
		/*HWND hEdit = GetDlgItem(hwnd, IDC_MAIN_EDIT);
		LoadTextFileToEdit(hEdit, szFileName);*/
	}
}


/****************
 * ABOUT DIALOG *
 ****************/
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch(Message)
	{
		case WM_INITDIALOG:

		return TRUE;
		case WM_CREATE:
			g_hbmBall = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BALL_ABOUT));
			if(g_hbmBall == NULL)
				MessageBox(hwnd, "Could not load IDB_BALL!", "Error", MB_OK | MB_ICONEXCLAMATION);
		break;
		case WM_PAINT:
		{
			// Just a note, never use a MessageBox from inside WM_PAINT
			// The box will cause more WM_PAINT messages and you'll probably end up
			// stuck in a loop

			BITMAP bm;
			PAINTSTRUCT ps;

			HDC hdc = BeginPaint(hwnd, &ps);

			HDC hdcMem = CreateCompatibleDC(hdc);
			HBITMAP hbmOld = SelectObject(hdcMem, g_hbmBall);

			GetObject(g_hbmBall, sizeof(bm), &bm);

			BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);

			SelectObject(hdcMem, hbmOld);
			DeleteDC(hdcMem);

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDOK_ABOUT:
					EndDialog(hwnd, IDOK_ABOUT);
				break;
			}
		break;
		default:
			return FALSE;
	}
	return TRUE;
}

/******************
 * OPTIONS DIALOG *
 ******************/
BOOL CALLBACK OptionsDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch(Message)
	{
		case WM_INITDIALOG:
            if (lower == TRUE)
                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO1);
            if (upper == TRUE)
                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO2);
            if (capitalize == TRUE)
                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO3);
            if (title == TRUE)
                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO4);
            if (spaces == TRUE)
                CheckDlgButton(hwnd, IDB_CHECK1, BST_CHECKED);
            if (underscores == TRUE)
                CheckDlgButton(hwnd, IDB_CHECK2, BST_CHECKED);
            if (merge_spaces == TRUE)
                CheckDlgButton(hwnd, IDB_CHECK3, BST_CHECKED);
            if (recurse == TRUE)
                CheckDlgButton(hwnd, IDB_CHECK4, BST_CHECKED);
		return TRUE;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
			    /* SAVE BUTTON */
				case IDSAVE_OPTIONS:
                    SetDlgItemText(hwnd, IDC_TEXT2, "Fuck");
					EndDialog(hwnd, IDSAVE_OPTIONS);
				break;

				/* LOWER RADIO BUTTON */
                case IDB_RADIO1:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_RADIO1, BM_GETCHECK, 0, 0) == 0)
                            {
                                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO1);

                                lower = TRUE; upper = FALSE; capitalize = FALSE; title = FALSE;

                                status[0] = 'L';
                            }
                        break;
                    }
                }
                break;

                /* UPPER RADIO BUTTON */
                case IDB_RADIO2:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_RADIO2, BM_GETCHECK, 0, 0) == 0)
                            {
                                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO2);

                                lower = FALSE; upper = TRUE; capitalize = FALSE; title = FALSE;

                                status[0] = 'U';
                            }
                        break;
                    }
                }
                break;

                /* CAPITALIZE RADIO BUTTON */
                case IDB_RADIO3:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_RADIO3, BM_GETCHECK, 0, 0) == 0)
                            {
                                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO3);

                                lower = FALSE; upper = FALSE; capitalize = TRUE; title = FALSE;

                                status[0] = 'C';
                            }
                        break;
                    }
                }
                break;

                /* TITLE RADIO BUTTON */
                case IDB_RADIO4:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_RADIO4, BM_GETCHECK, 0, 0) == 0)
                            {
                                CheckRadioButton(hwnd, IDB_RADIO1, IDB_RADIO4, IDB_RADIO4);

                                lower = FALSE; upper = FALSE; capitalize = FALSE; title = TRUE;

                                status[0] = 'T';
                            }
                        break;
                    }
                }
                break;

                /* SPACES CHECKBOX */
                case IDB_CHECK1:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_CHECK1, BM_GETCHECK, 0, 0)) {
                                spaces = TRUE;
                                status[1] = '+'; status[2] = 'S';
                            }
                            else {
                                spaces = FALSE;
                                status[1] = ' '; status[2] = ' ';
                            }
                        break;
                    }
                }
                break;

                /* UNDERSCORES CHECKBOX */
                case IDB_CHECK2:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_CHECK2, BM_GETCHECK, 0, 0)) {
                                underscores = TRUE;
                                status[1] = '+'; status[2] = 'u';
                            }
                            else {
                                underscores = FALSE;
                                status[1] = ' '; status[2] = ' ';
                            }
                        break;
                    }
                }
                break;

                /* MERGE SPACES CHECKBOX */
                case IDB_CHECK3:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_CHECK3, BM_GETCHECK, 0, 0)) {
                                merge_spaces = TRUE;
                                if (spaces == TRUE || underscores == TRUE)
                                    status[3] = 'M';
                                else
                                    status[1] = '+'; status[2] = 'M';
                            }
                            else {
                                merge_spaces = FALSE;
                                if (spaces == TRUE || underscores == TRUE)
                                    status[3] = ' ';
                                else
                                    status[1] = ' '; status[2] = ' ';
                            }
                        break;
                    }
                }
                break;

                /* RECURSE SUBDIRECTORIES CHECKBOX */
                case IDB_CHECK4:
                {
                    switch (HIWORD(wParam))
                    {
                        case BN_CLICKED:
                            if (SendDlgItemMessage(hwnd, IDB_CHECK4, BM_GETCHECK, 0, 0)) {
                                recurse = TRUE;
                                status[4] = '+'; status[5] = 'R';
                            }
                            else {
                                recurse = FALSE;
                                status[4] = ' '; status[5] = ' ';
                            }
                        break;
                    }
                }
                break;
			}
		break;

		default:
			return FALSE;
	}
	return TRUE;
}

/***************
 * MAIN DIALOG *
 ***************/
BOOL CALLBACK DlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch(Message)
	{
	    case WM_INITDIALOG:
            SetDlgItemText(hwnd, IDC_TEXT1, "caps v0.1 -- copygoat@hotmail.com");
            SetDlgItemText(hwnd, IDC_TEXT2, status);
        break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_FILE:
				{
                    HMENU FileMenu;
                    POINT pt;
                    GetCursorPos(&pt);
                    FileMenu = CreatePopupMenu();
                    AppendMenu(FileMenu,MF_STRING,ID_FILE_ADD_FILE,"Add File");
                    AppendMenu(FileMenu,MF_STRING,ID_FILE_ADD_FOLDER,"Add Folder");
                    AppendMenu(FileMenu,MF_STRING,ID_FILE_CLEAR,"Clear");
                    TrackPopupMenu(FileMenu,NULL,pt.x,pt.y,0,hwnd,NULL);
				}
				break;

				case ID_FILE_ADD_FILE:
				{
                    OPENFILENAME ofn;
                    char szFileName[MAX_PATH] = "";

                    ZeroMemory(&ofn, sizeof(ofn));

                    ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
                    ofn.hwndOwner = hwnd;
                    ofn.lpstrFilter = "All Files (*.*)\0*.*\0";
                    ofn.lpstrFile = szFileName;
                    ofn.nMaxFile = MAX_PATH;
                    ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
                    ofn.lpstrDefExt = "mp3";  // default extension :  if someone types
                                              // in "fuck" instead of "fuck.mp3"

                    if(GetOpenFileName(&ofn))
                    {
                        int index = SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)szFileName);
                    }
				}
				break;

				case ID_FILE_CLEAR:
                    SendDlgItemMessage(hwnd, IDC_LIST, LB_RESETCONTENT, 0, 0);
                break;

				case ID_FILE_ADD_FOLDER:
				{
                    BROWSEINFO bi;
                    LPITEMIDLIST Item;
                    // Ici, la taille du buffer ne peut pas �tre pass�e
                    // buffer est suppos� �tre de taille MAX_PATH (ou plus)
                    char buffer[MAX_PATH];

                    // On met tous les champs inutilis�s � 0
                    memset(&bi,0,sizeof(BROWSEINFO));
                    // hDlg est le HWND de la boite de dialogue qui demande l'ouverture
                    // Ou NULL si la boite de dialogue n'a pas de fen�tre parent
                    bi.hwndOwner=hwnd;
                    // Contient le r�pertoire initial ou NULL
                    bi.pidlRoot=NULL;
                    bi.pszDisplayName=buffer;
                    bi.lpszTitle="R�pertoire courant";
                    bi.ulFlags=NULL;
                    bi.lParam=NULL;
                    Item=SHBrowseForFolder(&bi);
                    if(Item!=NULL)
                    {
                        // buffer contient le nom du r�pertoire s�lectionn�
                        SHGetPathFromIDList(Item,buffer);
                        // buffer contient le chemin complet de la s�lection
                    }
				}
				break;

				case IDC_OPTIONS:
				{
				    int ret = DialogBox(GetModuleHandle(NULL),
						MAKEINTRESOURCE(IDD_OPTIONS), hwnd, OptionsDlgProc);
                    if(ret == -1){
						MessageBox(hwnd, "Dialog failed!", "Error",
							MB_OK | MB_ICONINFORMATION);
                    }
				}
				break;

				case IDC_ABOUT:
				{
				    int ret = DialogBox(GetModuleHandle(NULL),
						MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc);
                    if(ret == -1){
						MessageBox(hwnd, "Dialog failed!", "Error",
							MB_OK | MB_ICONINFORMATION);
                    }
                }
				break;

				case IDC_QUIT:
				{
                    EndDialog(hwnd, 0);
				}
				break;
			}
		break;
		case WM_CLOSE:
			EndDialog(hwnd, 0);
		break;
		default:
			return FALSE;
	}
	return TRUE;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, DlgProc);
}
