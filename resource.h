#define IDD_MAIN            101

/* program icon */
#define IDI_CAPS            102

/* main frame objects */
#define IDC_FILE            103
#define IDC_OPTIONS         104
#define IDC_ABOUT           105
#define IDC_QUIT            106
#define IDC_GO              107
#define IDC_TEXT1           108
#define IDC_TEXT2           109
#define IDC_LIST            110

/* about dialog objects */
#define IDD_ABOUT           201
#define IDC_STATIC_ABOUT    202
#define IDOK_ABOUT          203
#define IDB_BALL_ABOUT      204

/* options dialog objects */
#define IDD_OPTIONS         301
#define IDSAVE_OPTIONS      302
#define IDC_STATIC_CASE     303
#define IDB_RADIO1          304
#define IDB_RADIO2          305
#define IDB_RADIO3          306
#define IDB_RADIO4          307
#define IDB_CHECK1          308
#define IDB_CHECK2          309
#define IDB_CHECK3          310
#define IDB_CHECK4          311

/* file dialog objects */
#define IDR_FILE            401
#define ID_FILE_ADD_FILE    402
#define ID_FILE_ADD_FOLDER  403
#define ID_FILE_CLEAR       404
