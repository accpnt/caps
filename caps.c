#include "caps.h"

/* STRING OPERATIONS FUNCTIONS */
extern void lowerString(char* filename) {
	int stringLentgh = strlen(filename);
	int i;

	for (i=0;i<stringLentgh;i++) {
		filename[i] = tolower(filename[i]);
	}
}

extern void upperString(char* filename) {
	int stringLentgh = strlen(filename);
	int i;

	for (i=0;i<stringLentgh;i++) {
		filename[i] = toupper(filename[i]);
	}
}

extern void upper1stWord(char* filename) {
	int stringLentgh = strlen(filename);
	int i;

	filename[0] = toupper(filename[0]);
	for (i=1;i<stringLentgh;i++) {
		filename[i] = tolower(filename[i]);
	}
}

extern void lower1stLetters(char* filename) {
	int stringLentgh = strlen(filename);
	int i;
	int wordLetter = 1;

	for (i=0;i<stringLentgh;i++) {
		if ((isalnum(filename[i])) || (filename[i] == '.')) {
			if ((wordLetter == 1) && (islower(filename[i]))) {
				filename[i] = toupper(filename[i]);
			}
			if ((wordLetter > 1) && (isupper(filename[i]))) {
				filename[i] = tolower(filename[i]);
			}
			wordLetter++;
		}
		else {
			wordLetter = 1;
		}
	}
}

extern void addUnderscores(char* filename) {
	int stringLentgh = strlen(filename);
	int i;

	for (i=0;i<stringLentgh;i++) {
		if (isspace(filename[i]))
			filename[i] = '_';
	}
}

extern void removeUnderscores(char* filename) {
	int stringLentgh = strlen(filename);
	int i;

	for (i=0;i<stringLentgh;i++) {
		if (filename[i] == '_')
			filename[i] = ' ';
	}
}